#![windows_subsystem = "windows"]
use nannou::prelude::*;
use nannou::winit::window::Fullscreen::Borderless;

fn main() {
  nannou::app(model)
      .event(event)
      .run();
}

struct Model {
  _window: window::Id,
  lmb_down: bool,
  mmb_down: bool,
  rmb_down: bool,
  last_mouse_down: Point2<f32>,
  reference_size: f64,
}

fn model(app: &App) -> Model {
  app.set_exit_on_escape(false);
  let _window = app
      .new_window()
      .title("!squadruler")
      //.maximized(true)
      .fullscreen_with(Some(Borderless(app.primary_monitor())))
      .transparent(true)
      .decorations(false)
      .view(view)
      .build()
      .unwrap();
  Model {
    _window,
    lmb_down: false,
    mmb_down: false,
    rmb_down: false,
    last_mouse_down: (0.0, 0.0).into(),
    reference_size: 0.0,
  }
}

fn event(app: &App, model: &mut Model, event: Event) {
  match event {
    Event::WindowEvent { id: _id, simple} => {
      if let Some(window_event) = simple {
        match window_event {
          MousePressed(button) => {
            if button == MouseButton::Left{
              model.lmb_down = true;
              model.last_mouse_down = app.mouse.position();
            }
            if button == MouseButton::Middle{
              model.mmb_down = true;
              model.last_mouse_down = app.mouse.position();
            }
            if button == MouseButton::Right{
              model.rmb_down = true;
              model.last_mouse_down = app.mouse.position();
            }
          }
          MouseReleased(button) => {
            if button == MouseButton::Left{
              model.lmb_down = false;
            }
            if button == MouseButton::Middle{
              model.mmb_down = false;
              let x_delta = model.last_mouse_down.x - app.mouse.x;
              let y_delta = model.last_mouse_down.y - app.mouse.y;
              model.reference_size = f64::sqrt((x_delta*x_delta + y_delta*y_delta) as f64) / 3.0;
            }
            if button == MouseButton::Right{
              model.rmb_down = false;
              let x_delta = model.last_mouse_down.x - app.mouse.x;
              let y_delta = model.last_mouse_down.y - app.mouse.y;
              model.reference_size = f64::sqrt((x_delta*x_delta + y_delta*y_delta) as f64);
            }
          }
          _ => {}
        }
      }
    }
    _ => {}
  }
}

fn view(app: &App, model: &Model, frame: Frame) {
  let draw = app.draw();
  draw.background().rgba(0.0, 0.0, 0.0, 0.3);
  if model.lmb_down{
    let x_delta = model.last_mouse_down.x - app.mouse.x;
    let y_delta = model.last_mouse_down.y - app.mouse.y;
    let current_dist = f64::sqrt((x_delta*x_delta + y_delta*y_delta) as f64);

    let factor = if model.reference_size != 0.0 {current_dist / model.reference_size} else {0.0};
    let mouse_text = format!("{:.1}m", factor * 100.0);
    draw.line().stroke_weight(4.0).points(model.last_mouse_down, app.mouse.position()).color(rgb(1.0, 1.0, 0.0));
    draw.text(mouse_text.as_str()).x(app.mouse.x).y(app.mouse.y + 20.0).color(rgb(0.3, 0.3, 1.0)).font_size(30);

  }
  if model.mmb_down{
    let mouse_text = "set 300m";
    draw.line().stroke_weight(4.0).points(model.last_mouse_down, app.mouse.position()).color(rgb(0.3, 1.0, 0.3));
    draw.text(mouse_text).x(app.mouse.x).y(app.mouse.y + 20.0).color(rgb(1.0, 1.0, 0.0)).font_size(30);
  }
  if model.rmb_down{
    let mouse_text = "set 100m";
    draw.line().stroke_weight(4.0).points(model.last_mouse_down, app.mouse.position()).color(rgb(0.3, 0.3, 1.0));
    draw.text(mouse_text).x(app.mouse.x).y(app.mouse.y + 20.0).color(rgb(1.0, 1.0, 0.0)).font_size(30);
  }
  draw.to_frame(app, &frame).unwrap();
}